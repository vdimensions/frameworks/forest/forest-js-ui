import peerDepsExternal from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import copy from "rollup-plugin-copy";
import { prepareChunks, embedLicense } from "@vdimensions/rollup-js-helpers";

export default {
  input: prepareChunks( { workDir: `${__dirname}/src`, paths: [ "**/index.ts" ] }),
  output: [
    {
      dir: `dist`,
      format: "esm",
      sourcemap: true
    }
  ],
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({ 
      useTsconfigDeclarationDir: true, 
      rollupCommonJSResolveHack: false,
      clean: true
    }),
    copy({
      targets: [
        {
          src: "package.json",
          dest: "dist"
        }
      ]
    }),
    embedLicense({ workDir: __dirname}),
  ]
};
